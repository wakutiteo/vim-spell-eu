# vim-spell-eu

Basque spell file for Vim.

### Instalazioa

Spell fitxategia deskargatzen dugu:

```bash
mkdir -p ~/.vim/spell
wget https://gitlab.com/wakutiteo/vim-spell-eu/-/raw/master/eu.utf-8.spl -P ~/.vim/spell
```

> `eu.utf-8.spl` fitxategia "Xuxen 5.1 (Hunspell)" bertsioarekin sortu da.

Deskargatu ondoren, euskarazko zuzentzailea bi modutan ezar dezakegu:

1. Unean-unean, erabili nahi duzun bakoitzean:

    ```vim
    :set spell spelllang=eu
    ```

2. Etengabe, lehenetsita. Lerro hauek `~/.vimrc`-ri gehituz:

    ```vim
    set spell
    set spelllang=eu
    ```

## Spell fitxategia sortu

1. [Xuxen-etik](http://xuxen.eus/eu/deskargatu) Hunspell fitxategi konprimatua [deskargatu](http://xuxen.eus/static/hunspell/xuxen_5.1_hunspell.zip).

2. Deskonprimatzen dugu:

```bash
unzip xuxen_*_hunspell.zip
```

3. Vim ireki eta spell fitxategia sortu:

```vim
:mkspell eu eu_ES
```

Prozesua 20-22 ordu inguru irauten du. Prozesua bukatzean, gure `eu.utf-8.spl` fitxategia izango dugu.

> Spell fitxategi bat sortzeko moduari buruzko dokumentazio guztia hemen aurki daiteke: [spell-mkspell](https://vimdoc.sourceforge.net/htmldoc/spell.html#spell-mkspell).